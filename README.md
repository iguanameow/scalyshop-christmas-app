# Christmas-App

Christmas App Helm Kubernetes

## Connecting Projects

+ ScalyShop Frontend - **[hgitlab.com/iguanameow/scalyshop-frontend](https://gitlab.com/iguanameow/scalyshop-frontend)**
+ ScalyShop Backend - **[gitlab.com/iguanameow/scalyshop-backend](https://gitlab.com/iguanameow/scalyshop-backend)**
+ ScalyShop Cluster Management - **[gitlab.com/iguanameow/scalyshop-cluster-management](https://gitlab.com/iguanameow/scalyshop-cluster-management)**